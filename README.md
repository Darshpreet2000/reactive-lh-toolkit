### Run cassandra

 * In a terminal or a cmd, cd to the project folder and enter  
 `docker-compose up`  
 This will start a cassandra instance at _0.0.0.0/9042_
 * Port and host can be changed in the docker-compose.yml

### Run the server

 * In a terminal or a cmd, cd to the project folder and enter  
 `./mvnw spring-boot:run` or `.\mvnw.bat spring-boot:run` (on Windows)
 
Access the app by going to **localhost:8080/Patient**
