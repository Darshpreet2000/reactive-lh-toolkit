package org.openmrs.converter;

import org.hl7.fhir.dstu3.model.Meta;
import org.openmrs.annotation.CassandraConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

@Component
@CassandraConverter
@ReadingConverter
public class MetaConverter implements Converter<String, Meta> {

  @Override
  public Meta convert(String source) {
    return ConvertUtils.getInstance().fromString(source, Meta.class);
  }
}
